import argparse
import numpy as np
import matplotlib.pyplot as plt
import os

from scipy.interpolate import spline

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-d', '--directory', default='./')
args = arg_parser.parse_args()
mydir = args.directory
samples_dict = {'Scene': 22500, 'Tangram': 40000, 'Alchemy':31500}
fig = plt.figure(figsize=(16, 4))
domains_cnt = len(os.listdir(mydir))
for i, domain in enumerate(os.listdir(mydir)):
    samples = samples_dict[domain]
    print(domain)
    ax = fig.add_subplot(1, domains_cnt, (i+1), axisbg='white')
    plots = []
    experiments = []
    domain_path = os.path.join(mydir, domain)
    for exp in os.listdir(domain_path):
        plt_size = (1 + int(np.math.ceil(float(samples) / 300)))
        exp_path = os.path.join(domain_path, exp)
        acc = []
        #print("-----")
        for file in os.listdir(exp_path):
            run_path = os.path.join(exp_path, file)
            data = np.genfromtxt(run_path, delimiter=',', dtype = float)
            steps = [row[1] for row in data]
            steps_trunc = [x*300 for x in range(0, plt_size)]
            run_acc = [row[2] for row in data][:plt_size]
            #print(run_acc[1:5])
            acc.append(run_acc)
        #print("----------")
        std = np.array(acc).std(axis=0)[1:]
        #print(std[:5])
        avg = np.array(acc).mean(axis=0)[1:]
        sparse_steps = [step for i, step in enumerate(steps_trunc[1:]) if (i % 2 == 0)]
        sparse_avg = [acc for i, acc in enumerate(avg) if (i % 2 == 0)]
        sparse_std = [acc for i, acc_std in enumerate(std) if (i % 2 == 0)]
        steps_np = np.array(sparse_steps)
        steps_trunc_smooth = np.linspace(steps_np.min(), steps_np.max(), 100*len(steps_trunc[1:]))
        avg_smooth = spline(steps_np, sparse_avg, steps_trunc_smooth)
        std_smooth = spline(steps_np, sparse_std, steps_trunc_smooth)
        plot = ax.plot(steps_trunc_smooth, avg_smooth, lw=1.3, label=exp)
        experiments.append(exp)
        plots.append(plot[0])

    plt.title(domain)
    plt.legend(plots, experiments)
    plt.xlabel('Train step')
    plt.ylabel('Train hit accuracy')
plt.tight_layout()
plt.savefig(mydir + "/plots.pdf")
plt.show()
