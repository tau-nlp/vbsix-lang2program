import os
import fnmatch
import argparse
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('-e', '--experiment', default='0')
args = arg_parser.parse_args()
exp_path = "./data/experiments/" + args.experiment + "/"
exp_path = os.path.abspath(exp_path)
print(exp_path)
checkpoints_path = os.path.join(exp_path, 'checkpoints')
for root, dirs, files in os.walk(checkpoints_path):
    for basename in files:
        if fnmatch.fnmatch(basename, '*.index'):
            filename = os.path.join(root, basename)
            checkpoint = os.path.splitext(filename)[0]
            checkpoint_file = checkpoints_path + "/checkpoint"
            chk_file = open(checkpoint_file, 'w+')
            chk_file.write("model_checkpoint_path: \"" + checkpoint + "\"\n")
            chk_file.write("all_model_checkpoint_paths: \"" + checkpoint + "\"")
            chk_file.close()
            break

