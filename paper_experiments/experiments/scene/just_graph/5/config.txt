seed = 0
train_mode = "semi-supervised"
delexicalized = false
optimizer = "adam"
learning_rate = 0.001
batch_size = 8
max_iters = 1000000
train_batch_size = 5000
num_evaluate_examples = 150
num_evaluate_examples_big = 100000000000000
rollout = false
timing {
  save = 1000
  eval = 300
  big_eval = 4500
}
parse_model {
  train_word_embeddings = false
  utterance_embedder {
    lstm_dim = 64
    utterance_length = 25
  }
  condition_on_history = false
  history_length = 4
  condition_on_stack = true
  stack_embedder {
    max_list_size = 7
    primitive_dim = 32
    object_dim = 32
  }
  soft_copy = false
  predicate_positions = false
  h_dims = [
    128
    128
  ]
}
decoder {
  inputs_caching = false
  joint_training = false
  optimizer = "adam"
  learning_rate = 0.001
  rollout = false
  prune {
    max_stack_size = 3
    action_must_clear_beam = true
  }
  normalization = "local"
  train_exploration_policy {
    type = "graph-beam-search"
    exploration_gamma = null
    exploration_epsilon = 0.15
    beam_size = 32
    independent_utterance_exploration = false
    iterations_per_utterance = 7
    correct_beam_size = 8
    value_weight = 0
  }
  test_exploration_policy {
    type = "beam-search"
    exploration_epsilon = 0
    exploration_gamma = 1
    beam_size = 32
    independent_utterance_exploration = false
    iterations_per_utterance = 7
  }
  rollout_policy {
    type = "batched-reinforce"
    exploration_gamma = 1.0
    exploration_epsilon = 0
    beam_size = 128
    independent_utterance_exploration = false
    iterations_per_utterance = 7
    termination_lookahead = false
    zombie_mode = false
  }
  case_weighter {
    type = "mml"
    alpha = 1
    beta = 1
  }
  value_function {
    type = "constant"
    constant_value = 0
    sub_domain = "scene"
  }
  expert_policy {
    use_expert_policy = false
  }
}
dataset {
  domain = "rlong"
  train_num_steps = [
    4
    5
  ]
  valid_num_steps = [
    3
    5
  ]
  final_num_steps = [
    3
    5
  ]
  train_slice_steps_from_middle = false
  valid_slice_steps_from_middle = false
  final_slice_steps_from_middle = false
  name = "scene"
  train_file = "rlong/scene-train.tsv"
  valid_file = "rlong/scene-dev.tsv"
  final_file = "rlong/scene-test.tsv"
}