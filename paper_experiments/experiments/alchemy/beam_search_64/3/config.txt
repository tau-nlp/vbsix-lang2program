seed = 0
train_mode = "semi-supervised"
delexicalized = false
optimizer = "adam"
learning_rate = 0.001
batch_size = 8
max_iters = 1000000
train_batch_size = 5000
num_evaluate_examples = 150
num_evaluate_examples_big = 10000000000000
timing {
  save = 1000
  eval = 300
  big_eval = 4500
}
parse_model {
  train_word_embeddings = false
  utterance_embedder {
    lstm_dim = 64
    utterance_length = 18
  }
  condition_on_history = false
  history_length = 4
  condition_on_stack = true
  stack_embedder {
    max_list_size = 7
    primitive_dim = 32
    object_dim = 32
  }
  soft_copy = false
  predicate_positions = false
  h_dims = [
    128
    128
  ]
}
decoder {
  inputs_caching = false
  prune {
    max_stack_size = 3
    action_must_clear_beam = true
  }
  normalization = "local"
  train_exploration_policy {
    type = "beam-search"
    exploration_gamma = null
    exploration_epsilon = 0.15
    beam_size = 64
    independent_utterance_exploration = false
    iterations_per_utterance = 7
  }
  test_exploration_policy {
    type = "beam-search"
    exploration_epsilon = 0
    exploration_gamma = 1
    beam_size = 32
    independent_utterance_exploration = false
    iterations_per_utterance = 7
  }
  case_weighter {
    type = "mml"
    alpha = 1
    beta = 1
  }
  value_function {
    type = "constant"
    constant_value = 0
  }
}
dataset {
  domain = "rlong"
  train_num_steps = [
    1
    5
  ]
  valid_num_steps = [
    3
    5
  ]
  final_num_steps = [
    3
    5
  ]
  train_slice_steps_from_middle = false
  valid_slice_steps_from_middle = false
  final_slice_steps_from_middle = false
  name = "alchemy"
  train_file = "rlong/alchemy-train.tsv"
  valid_file = "rlong/alchemy-dev.tsv"
  final_file = "rlong/alchemy-test.tsv"
}