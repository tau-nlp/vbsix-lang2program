import os
import random
from itertools import izip

import numpy as np
import tensorflow as tf

import gtd.ml.experiment
from dependency.data_directory import DataDirectory
from gtd.chrono import verboserate
from gtd.ml.model import TokenEmbedder
from gtd.ml.utils import guarantee_initialized_variables
from gtd.utils import cached_property, as_batches, random_seed, sample_if_large
from strongsup.decoder import Decoder
from strongsup.domain import get_domain
from strongsup.embeddings import (
        StaticPredicateEmbeddings, GloveEmbeddings, TypeEmbeddings,
        RLongPrimitiveEmbeddings)
from strongsup.evaluation import Evaluation, BernoulliSequenceStat
from strongsup.example import Example, DelexicalizedContext
from strongsup.parse_case import ParsePath
from strongsup.parse_model import (
    UtteranceEmbedder,
    UtteranceEmbedderFut,
    CombinedPredicateEmbedder, DynamicPredicateEmbedder,
    PositionalPredicateEmbedder, DelexicalizedDynamicPredicateEmbedder,
    HistoryEmbedder,
    SimplePredicateScorer, AttentionPredicateScorer,
    SoftCopyPredicateScorer, PredicateScorer,
    CrossEntropyLossModel, LogitLossModel,
    ParseModel, TrainParseModel,
    ExecutionStackEmbedder, RLongObjectEmbedder)
from strongsup.utils import OptimizerOptions
from strongsup.value_function import ValueFunctionExample
from strongsup.visualizer import Visualizer
from strongsup.value_function import build_world_embedder


class Experiments(gtd.ml.experiment.Experiments):
    def __init__(self, check_commit=True):
        """Create Experiments.

        If check_commit is true, this will not allow you to run old experiments
        without being on the correct commit number, or old experiments where
        the working directory was not clean.
        """
        data_dir = DataDirectory.experiments
        src_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        default_config = os.path.join(src_dir, 'configs', 'debug.txt')
        super(Experiments, self).__init__(data_dir, src_dir, Experiment, default_config, check_commit=check_commit)

def stack_to_str(self, exec_stack):
    str_elem = "["
    i = 0
    for elem in exec_stack:
        if isinstance(elem, list):
            if len(elem) == 1:
                str_elem = str_elem + str(elem[0])
            else:
                str_elem = str_elem + str(elem)
        else:
            str_elem = str_elem + str(elem)
        i = i + 1
        if i < len(exec_stack):
            str_elem = str_elem + ", "
    str_elem = str_elem + "]"
    return str_elem

class Experiment(gtd.ml.experiment.TFExperiment):
    """Encapsulates the elements of a training run."""

    def __init__(self, config, save_dir):
        super(Experiment, self).__init__(config, save_dir)
        self.workspace.add_file('train_visualize', 'train_visualizer.txt')
        self.workspace.add_file('valid_visualize', 'valid_visualizer.txt')
        self.workspace.add_file('full_eval', 'full_eval_at_{step}.txt')
        self.workspace.add_dir('eval_trained', 'eval_trained')
        self.workspace.add_file('trained_eval', 'eval_trained/trained_{name}.txt')
        self.workspace.add_file('trained_visualize', 'eval_trained/trained_visualizer.txt')
        self.workspace.add_file('codalab', 'codalab.json')
        self._domain = get_domain(config)

        self._train_parse_model = self._build_train_parse_model()
        self._decoder = self._build_decoder(self.train_parse_model)

        self._train_visualizer = Visualizer(self.decoder, self.workspace.train_visualize,
                                'train', train=True)

        self._valid_visualizer = Visualizer(self.decoder, self.workspace.valid_visualize,
                                'valid', train=False)

        # Reload weights if they exist. Otherwise, initialize weights.
        try:
            self.saver.restore()
            print 'Successfully reloaded the weights'
        except IOError:
            # NOTE: use this instead of tf.initialize_all_variables()!
            # That op will overwrite Keras initializations.
            sess = tf.get_default_session()
            guarantee_initialized_variables(sess)
            print 'Weights initialized'

    @property
    def train_parse_model(self):
        return self._train_parse_model

    @property
    def parse_model(self):
        return self.train_parse_model.parse_model

    @property
    def decoder(self):
        return self._decoder

    @property
    def path_checker(self):
        return self._domain.path_checker

    @property
    def train_visualizer(self):
        return self._train_visualizer

    @property
    def valid_visualizer(self):
        return self._valid_visualizer

    def _build_train_parse_model(self):
        """Construct the TrainParseModel.

        If weights have been saved to disk, restore those weights.

        Returns:
            TrainParseModel
        """
        config = self.config.parse_model
        delexicalized = self.config.delexicalized

        # Glove embeddings have embed_dim 100
        glove_embeddings = GloveEmbeddings(vocab_size=20000)
        type_embeddings = TypeEmbeddings(embed_dim=50, all_types=self._domain.all_types)

        # set up word embeddings
        word_embedder = TokenEmbedder(glove_embeddings, 'word_embeds', trainable=config.train_word_embeddings)
        type_embedder = TokenEmbedder(type_embeddings, 'type_embeds')

        # build utterance embedder
        utterance_embedder = UtteranceEmbedder(word_embedder, lstm_dim=config.utterance_embedder.lstm_dim,
                                               utterance_length=config.utterance_embedder.utterance_length)

        # build future utterance embedder
        word_embedder_fut = TokenEmbedder(glove_embeddings, 'word_embeds_fut', trainable=config.train_word_embeddings)
        utterance_embedder_fut = UtteranceEmbedderFut(word_embedder_fut, lstm_dim=config.utterance_embedder.lstm_dim,
                                                      utterance_length=config.utterance_embedder.utterance_length)

        # build predicate embedder

        # dynamic
        if delexicalized:
            dyn_pred_embedder = DelexicalizedDynamicPredicateEmbedder(utterance_embedder.hidden_states_by_utterance,
                                                                      type_embedder)
        else:
            dyn_pred_embedder = DynamicPredicateEmbedder(word_embedder, type_embedder)
            if config.predicate_positions:
                dyn_pred_embedder = PositionalPredicateEmbedder(dyn_pred_embedder)

        # static
        static_pred_embeddings = StaticPredicateEmbeddings(
                dyn_pred_embedder.embed_dim,   # matching dim
                self._domain.fixed_predicates)
        static_pred_embedder = TokenEmbedder(static_pred_embeddings, 'static_pred_embeds')

        # combined
        pred_embedder = CombinedPredicateEmbedder(static_pred_embedder, dyn_pred_embedder)

        # build history embedder
        if config.condition_on_history:
            history_embedder = HistoryEmbedder(pred_embedder, config.history_length)
        else:
            history_embedder = None

        # build answer embedder
        if self.config.decoder.expert_policy.use_expert_policy:
            answer_embedder = build_world_embedder(self.config.decoder.value_function, name='target_primitive_embeds_ans')
        else:
            answer_embedder = None

        # build execution stack embedder
        if config.condition_on_stack:
            max_stack_size = self.config.decoder.prune.max_stack_size
            max_list_size = config.stack_embedder.max_list_size
            primitive_dim = config.stack_embedder.primitive_dim
            object_dim = config.stack_embedder.object_dim

            primitive_embeddings = RLongPrimitiveEmbeddings(primitive_dim)
            stack_primitive_embedder = TokenEmbedder(primitive_embeddings, 'primitive_embeds', trainable=True)

            # TODO(kelvin): pull this out as its own method
            assert self.config.dataset.domain == 'rlong'
            sub_domain = self.config.dataset.name
            attrib_extractors = [lambda obj: obj.position]
            if sub_domain == 'scene':
                attrib_extractors.append(lambda obj: obj.shirt)
                attrib_extractors.append(lambda obj: obj.hat)
                pass
            elif sub_domain == 'alchemy':
                # skipping chemicals attribute for now, because it is actually a list
                attrib_extractors.append(lambda obj: obj.color if obj.color is not None else 'color-na')
                attrib_extractors.append(lambda obj: obj.amount)
            elif sub_domain == 'tangrams':
                attrib_extractors.append(lambda obj: obj.shape)
            elif sub_domain == 'undograms':
                attrib_extractors.append(lambda obj: obj.shape)
            else:
                raise ValueError('No stack embedder available for sub-domain: {}.'.format(sub_domain))

            stack_object_embedder = RLongObjectEmbedder(attrib_extractors, stack_primitive_embedder,
                                                        max_stack_size, max_list_size)

            stack_embedder = ExecutionStackEmbedder(stack_primitive_embedder, stack_object_embedder,
                                                    max_stack_size=max_stack_size,  max_list_size=max_list_size,
                                                    project_object_embeds=True, abstract_objects=False)
        else:
            stack_embedder = None

        def scorer_factory(query_tensor):
            simple_scorer = SimplePredicateScorer(query_tensor, pred_embedder)
            attention_scorer = AttentionPredicateScorer(query_tensor, pred_embedder, utterance_embedder)
            soft_copy_scorer = SoftCopyPredicateScorer(attention_scorer.attention_on_utterance.logits,
                                                       disable=not config.soft_copy
                                                       )  # note that if config.soft_copy is None, then soft_copy is disabled
            scorer = PredicateScorer(simple_scorer, attention_scorer, soft_copy_scorer)
            return scorer

        parse_model = ParseModel(pred_embedder, history_embedder, stack_embedder,
                utterance_embedder, utterance_embedder_fut, answer_embedder, scorer_factory, config.h_dims,
                self._domain, delexicalized)

        if self.config.decoder.normalization == 'local':
            loss_model_factory = CrossEntropyLossModel
        else:
            loss_model_factory = LogitLossModel
        train_parse_model = TrainParseModel(parse_model, loss_model_factory,
                self.config.learning_rate,
                OptimizerOptions(self.config.optimizer),
                self.config.get('train_batch_size'))

        return train_parse_model

    def _build_decoder(self, train_parse_model):
        return Decoder(train_parse_model, self.config.decoder, self._domain)

    @cached_property
    def _examples(self):
        train, valid, final = self._domain.load_datasets()

        def delexicalize_examples(examples):
            delex_examples = []
            for ex in examples:
                delex_context = DelexicalizedContext(ex.context)
                delex_ex = Example(delex_context, answer=ex.answer, logical_form=ex.logical_form)
                delex_examples.append(delex_ex)
            return delex_examples

        if self.config.delexicalized:
            train = delexicalize_examples(train)
            valid = delexicalize_examples(valid)
            final = delexicalize_examples(final)
        return train, valid, final

    @property
    def train_examples(self):
        return self._examples[0]

    @property
    def valid_examples(self):
        return self._examples[1]

    @property
    def final_examples(self):
        return self._examples[2]

    def train(self):
        decoder = self.decoder
        eval_steps = self.config.timing.eval
        big_eval_steps = self.config.timing.big_eval
        save_steps = self.config.timing.save
        self.evaluate(step=decoder.step)  # evaluate once before training begins

        while True:
            train_examples = random.sample(self.train_examples, k=len(self.train_examples))  # random shuffle
            train_examples = verboserate(train_examples, desc='Streaming training Examples')
            for example_batch in as_batches(train_examples, self.config.batch_size):
                decoder.train_step(example_batch)
                step = decoder.step

                self.report_cache_stats(step)
                if (step + 1) % save_steps == 0:
                    self.saver.save(step)
                if (step + 1) % eval_steps == 0:
                    self.evaluate(step)
                if (step + 1) % big_eval_steps == 0:
                    self.big_evaluate(step)
                if step >= self.config.max_iters:
                    self.evaluate(step)
                    self.saver.save(step)
                    return

    # def supervised_train(self):
    #     train_parse_model = self.train_parse_model
    #     eval_time = Pulse(self.config.timing.eval)
    #     supervised_eval_time = Pulse(self.config.timing.supervised_eval)
    #     cases = examples_to_supervised_cases(self.train_examples)
    #     while True:
    #         for case_batch in as_batches(cases, self.config.batch_size):
    #             weights = [1.0 / len(case_batch)] * len(case_batch)
    #             train_parse_model.train_step(case_batch, weights, self.config.decoder.inputs_caching)
    #             step = train_parse_model.step
    #             self.report_cache_stats(step)
    #             self.saver.interval_save(step, self.config.timing.save)
    #             if eval_time():
    #                 self.evaluate(step)
    #                 eval_time.reset()
    #             if supervised_eval_time():
    #                 self.supervised_evaluate(step)
    #                 supervised_eval_time.reset()
    #             if step >= self.config.max_iters:
    #                 self.evaluate(step)
    #                 self.saver.save(step)
    #                 return

    def report_cache_stats(self, step):
        return              # Don't log these
        parse_model = self.parse_model
        scorer_cache = parse_model._scorer.inputs_to_feed_dict_cached
        pred_cache = parse_model._pred_embedder.inputs_to_feed_dict_cached
        self.tb_logger.log('cache_scorer_size', scorer_cache.cache_size, step)
        self.tb_logger.log('cache_predEmbedder_size', pred_cache.cache_size, step)
        self.tb_logger.log('cache_scorer_hitRate', scorer_cache.hit_rate, step)
        self.tb_logger.log('cache_predEmbedder_hitRate', pred_cache.hit_rate, step)

    def supervised_evaluate(self, step):
        train_parse_model = self.train_parse_model

        def case_sample(examples):
            """Get a random sample of supervised ParseCases."""
            with random_seed(0):
                example_sample = sample_if_large(examples, 30)
            return list(examples_to_supervised_cases(example_sample))

        def report_loss(cases, name):
            weights = [1.0] * len(cases)
            loss = train_parse_model.compute(train_parse_model.loss, cases, weights,
                    caching=self.config.decoder.inputs_caching)
            self.tb_logger.log(name, loss, step)

        report_loss(case_sample(self.train_examples), 'loss_train')
        report_loss(case_sample(self.valid_examples), 'loss_val')

    def evaluate(self, step):
        print 'Evaluate at step {}'.format(step)
        num_examples = self.config.num_evaluate_examples
        with random_seed(0):
            train_sample = sample_if_large(self.train_examples, num_examples,
                    replace=False)
        with random_seed(0):
            valid_sample = sample_if_large(self.valid_examples, num_examples,
                    replace=False)
        train_eval = self.evaluate_on_examples(step, train_sample, self.train_visualizer)
        valid_eval = self.evaluate_on_examples(step, valid_sample, self.valid_visualizer)

        # Log to TensorBoard
        train_eval.json_summarize(self.workspace.codalab, step)
        train_eval.tboard_summarize(self.tb_logger, step)
        valid_eval.json_summarize(self.workspace.codalab, step)
        valid_eval.tboard_summarize(self.tb_logger, step)

    def evaluate_on_examples(self, step, examples, visualizer):
        evaluation = Evaluation()
        examples = verboserate(examples, desc='Decoding {} examples'.format(visualizer.group_name))
        visualizer.reset(step=step)
        for ex_batch in as_batches(examples, self.config.batch_size):
            beams, batch_evaluation = visualizer.predictions(ex_batch)
            evaluation.add_evaluation(batch_evaluation)
            # collect value function examples
            value_function = self.decoder._value_function
            examples_to_vf_ex, vf_examples = ValueFunctionExample.examples_from_paths_batch(beams, ex_batch)
            predicted_values = value_function.values([ex.case for ex in vf_examples], [ex.answer for ex in vf_examples])
            rewards = np.array([ex.reward for ex in vf_examples])
            cases = dict()
            if visualizer.group_name == "train":
                cases['VF_Total'] = []
                for i, ex in enumerate(vf_examples):
                    example_len = len(ex.case.context.utterances)
                    utt_ind = ex.case.current_utterance_idx
                    cases['VF_Total'].append(i)
                    name = 'VF_{}_{}'.format(example_len, utt_ind+1)
                    if name not in cases:
                        cases[name] = []
                    cases[name].append(i)
            for case in cases:
                if len(predicted_values) == 0:
                    continue
                local_preds = predicted_values[cases[case]]
                local_rewards = rewards[cases[case]]
                if len(local_preds) == 0:
                    continue
                metrics = self.calc_metrics(local_preds, local_rewards)
                for metric in metrics:
                    score = metrics[metric]
                    if score is not None:
                        evaluation.add(case+'_{}_{}'.format(visualizer.group_name, metric), metrics[metric])

            avg_predicted_value = np.mean(predicted_values)
            avg_reward = np.mean([ex.reward for ex in vf_examples])

            evaluation.add('avgPredictedValue_{}'.format(visualizer.group_name), avg_predicted_value)
            evaluation.add('avgReward_{}'.format(visualizer.group_name), avg_reward)
        return evaluation

    def calc_metrics(self, values, rewards):
        metrices = dict()
        l2_loss = np.mean((values - rewards) ** 2)
        # label = np.round(rewards).astype(int)
        # pred = np.round(values).astype(int)

        label = np.where(rewards > 0, 1, 0)
        tiny_pred = np.where(values > 0.0001, 1, 0)
        small_pred = np.where(values > 0.001, 1, 0)
        medium_pred = np.where(values > 0.01, 1, 0)
        large_pred = np.where(values > 0.1, 1, 0)

        tiny_accuracy = np.mean(label == tiny_pred)
        tiny_recall_entries = small_pred[label == 1]
        tiny_precision_entries = label[tiny_pred == 1]
        tiny_recall = np.mean(tiny_recall_entries) if len(tiny_recall_entries) > 0 else None
        tiny_precision = np.mean(tiny_precision_entries) if len(tiny_precision_entries) > 0 else None

        small_accuracy = np.mean(label == small_pred)
        small_recall_entries = small_pred[label == 1]
        small_precision_entries = label[small_pred == 1]
        small_recall = np.mean(small_recall_entries) if len(small_recall_entries)>0 else None
        small_precision = np.mean(small_precision_entries) if len(small_precision_entries)>0 else None

        medium_accuracy = np.mean(label == medium_pred)
        medium_recall_entries = medium_pred[label == 1]
        medium_precision_entries = label[medium_pred == 1]
        medium_recall = np.mean(medium_recall_entries) if len(medium_recall_entries) > 0 else None
        medium_precision = np.mean(medium_precision_entries) if len(medium_precision_entries) > 0 else None

        large_accuracy = np.mean(label == large_pred)
        large_recall_entries = large_pred[label == 1]
        large_precision_entries = label[large_pred == 1]
        large_recall = np.mean(large_recall_entries) if len(large_recall_entries) > 0 else None
        large_precision = np.mean(large_precision_entries) if len(large_precision_entries) > 0 else None

        large_rewards = rewards[rewards > 0.7]
        large_values = values[rewards > 0.7]
        l2_loss_large = np.mean((large_values - large_rewards) ** 2) if len(large_rewards)>0 else None

        small_rewards = rewards[rewards > 0]
        small_values = values[rewards > 0]
        l2_loss_small = np.mean((small_values - small_rewards) ** 2) if len(small_rewards) > 0 else None

        metrices['l2_loss'] = l2_loss
        metrices['l2_loss_high_0.7'] = l2_loss_large
        metrices['l2_loss_high_pos'] = l2_loss_small
        metrices['accuracy_0.0001'] = tiny_accuracy
        metrices['recall_0.0001'] = tiny_recall
        metrices['precision_0.0001'] = tiny_precision
        metrices['accuracy_0.001'] = small_accuracy
        metrices['recall_0.001'] = small_recall
        metrices['precision_0.001'] = small_precision
        metrices['accuracy_0.01'] = medium_accuracy
        metrices['recall_0.01'] = medium_recall
        metrices['precision_0.01'] = medium_precision
        metrices['accuracy_0.1'] = large_accuracy
        metrices['recall_0.1'] = large_recall
        metrices['precision_0.1'] = large_precision
        return metrices

    def big_evaluate(self, step, num_samples=None):
        """Run more comprehensive evaluation of the model.

        How this differs from `self.evaluate`:
        - Compute confidence intervals for denotational accuracy estimates
        - Option to evaluate on a custom/larger number of samples
        - Due to the larger number of samples, don't print everything out to a visualizer.
        - Save stats to a file.

        Args:
            step (int)
            num_samples (# samples to evaluate on, for both train and test)
        """
        if num_samples is None:
            num_samples = self._config.num_evaluate_examples_big
        full_eval_path = self.workspace.full_eval.format(step=step)
        silent_visualizer = Visualizer(self.decoder, '/dev/null', 'silent', train=False)

        def evaluate_helper(examples, prefix):
            with random_seed(0):
                sample = sample_if_large(examples, num_samples, replace=False)
            eval = self.evaluate_on_examples(step=step, examples=sample, visualizer=silent_visualizer)

            # wrap with BernoulliSequenceStat, for conf intervals
            for name, stat in eval.stats.items():
                if name.startswith('denoAcc'):
                    eval.stats[name] = BernoulliSequenceStat(stat)

            with open(full_eval_path, 'a') as f:
                eval.summarize(f, prefix=prefix)

            eval.tboard_summarize(self.tb_logger, step, prefix=prefix)
            eval.json_summarize(self.workspace.codalab, step, prefix=prefix)

        evaluate_helper(self.final_examples, 'FINAL')
        evaluate_helper(self.valid_examples, 'VALID')

    def trained_evaluate(self, exp_name="my_exp", num_samples=None, visualize=True):
        """Add extra evaluations for some existing experiment.

        Args:
            exp_name (str) experiment title
            num_samples (# samples to evaluate on)
            visualize(bool) activates the visualizer log
        """
        step = 0
        if num_samples is None:
            num_samples = self._config.num_evaluate_examples_big
        trained_eval_path = self.workspace.trained_eval.format(name=exp_name)
        if visualize:
            trained_visualizer = Visualizer(self.decoder, self.workspace.trained_visualize,
                                                'trained', train=False)
        else:
            trained_visualizer = Visualizer(self.decoder, '/dev/null', 'silent', train=False)

        def evaluate_helper(examples, prefix):
            with random_seed(0):
                sample = sample_if_large(examples, num_samples, replace=False)
            eval = self.evaluate_on_examples(step=step, examples=sample, visualizer=trained_visualizer)

            # wrap with BernoulliSequenceStat, for conf intervals
            for name, stat in eval.stats.items():
                if name.startswith('denoAcc'):
                    eval.stats[name] = BernoulliSequenceStat(stat)

            with open(trained_eval_path, 'a') as f:
                eval.summarize(f, prefix=prefix)


            eval.tboard_summarize(self.tb_logger, step, prefix=prefix)
            eval.json_summarize(self.workspace.codalab, step, prefix=prefix)
        evaluate_helper(self.final_examples, 'FINAL')
        evaluate_helper(self.valid_examples, 'VALID')

def example_to_supervised_cases(example):
    """Convert Example to a list of supervised ParseCases.

    Only possible if example.logical_form is known.

    Args:
        example (Example)

    Returns:
        list[ParseCase]
    """
    path = ParsePath([], context=example.context)
    predicates = example.logical_form
    cases = []
    for pred in predicates:
        case = path.extend()
        if pred not in case.choices:
            case.choices.append(pred)
        case.decision = pred
        cases.append(case)
        path = case.path
    return cases


def examples_to_supervised_cases(examples):
    """Return a Generator of supervised ParseCases."""
    for example in verboserate(examples, desc='Streaming supervised ParseCases'):
        for case in example_to_supervised_cases(example):
            yield case
