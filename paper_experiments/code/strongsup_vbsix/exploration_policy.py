from enum import Enum

import numpy as np

from collections import Sequence, Counter

from abc import ABCMeta, abstractmethod

import time

from gtd.chrono import verboserate
from gtd.utils import flatten
from strongsup.parse_case import ParseCase, ParsePath
from strongsup.utils import epsilon_greedy_sample, softmax
from strongsup.utils import sample_with_replacement
from strongsup.decoder import NormalizationOptions
from math import log, sqrt
from collections import deque

from strongsup.value import check_denotation


class Beam(Sequence):
    """A Sequence of ParsePaths.
    In each ParsePath, each ParseCase must have already have a decision.

    Usually paths in a Beam are unique, but this is not required
    (e.g., BatchedReinforce uses Beams with repeated paths).
    """
    __slots__ = ['_paths']

    @classmethod
    def initial_beam(self, context):
        """Return the initial beam for the context.

        Args:
            context (Context)

        Returns:
            Beam
        """
        return Beam([ParsePath.empty(context)])

    def __init__(self, paths):
        self._paths = paths

    def __getitem__(self, i):
        return self._paths[i]

    def __len__(self):
        return len(self._paths)

    def __str__(self):
        return 'Beam' + str(self._paths)

    __repr__ = __str__

    def append(self, path):
        self._paths.append(path)

    @property
    def terminated(self):
        """Whether all paths are terminated."""
        return all(path.terminated for path in self._paths)

    def get_terminated(self):
        """Get only the terminated paths."""
        return Beam([path for path in self._paths if path.terminated])


def get_num_iterations(iterations_per_utterance, examples):
    """Returns the number of iterations to run for in this batch of examples

    Args:
        iterations_per_utterance (int): iterations per utterance config
        examples (list[Example])

    Returns:
        int: number of iterations
    """
    return iterations_per_utterance * max(
        [len(ex.context.utterances) for ex in examples])


class PathValue(object):
    def __init__(self, path, value):
        self.path = path
        self.value = value


class ExplorationPolicy(object):
    """For given examples, search for candidate ParseCase based on some
    exploration policy.

    An ExplorationPolicy will be called by the decoder.
    Since Examples are passed in, an ExplorationPolicy can choose to 'cheat'
    and use the answer or gold logical form to aid the exploration.
    This is totally fine during training.
    """
    __metaclass__ = ABCMeta

    def __init__(self, decoder, config, normalization, train):
        """
        Args:
            decoder (Decoder)
            config (Config)
            normalization (NormalizationOptions)
            train (bool): train or test policy
        """
        self._decoder = decoder
        self._config = config
        self._normalization = normalization
        self._train = train

    @abstractmethod
    def get_beams(self, examples, verbose=False):
        """Return a beam of scored ParseCases for each example.

        Args:
            examples (list[Example]): List of examples
            verbose (bool): Verbosity
        Returns:
            list[Beam] of length len(examples).
        """
        raise NotImplementedError

    @abstractmethod
    def get_intermediate_beams(self, examples, verbose=False):
        """Return the final beam along with intermediate beams / exploration states.

        Args:
            examples (list[Example]): List of examples
            verbose (bool): Verbosity
        Returns:
            list[Beam], list[list[Beam]]
            Each list has length len(examples).
            Each sublist i in the second output contains the intermediate beams
                for example i.
        """
        raise NotImplementedError

    def _ranker(self, path):
        """Assigns a score to a ParsePath depending on the configs

        Return the log unnormalized probability of the ParsePath.
        The returned value can be used to rank ParsePaths.

        For local normalization, the method returns the log-probability.
        For global normalization, the method returns the cumulative logit.

        Args:
            path (ParsePath): path to be scored

        Return:
            float: the score
        """
        if self._normalization == NormalizationOptions.LOCAL:
            return path.log_prob
        elif self._normalization == NormalizationOptions.GLOBAL:
            return path.score
        else:
            raise ValueError(
                'Unknown normalization type: {}'.format(self._normalization))

    def _q_ranker(self, path_value):
        path_prob = np.exp(path_value.path.log_prob)
        value = path_value.value
        if value == -1:
            value_weight = 0
        else:
            value_weight = self._config.value_weight
        rank = (value_weight * value) + (1 - value_weight) * path_prob
        return rank


################################
# Search Graph


class StateNode(object):
    def __init__(self):
        """A state in the execution space (stack + utterances + command history + world state).
            - state (str): string representation of the state
            - rep_path (ParsePath): some path which represents the node's state
            - rep_case (ParseCase): the last case of rep_path
            - children (dict{str: StateNode}): Search-space edges: states of the prefixes' continuations
            - parents (dict{str: StateNode}): Inverse of the children edges
            - terminated (bool): is this a terminal state?
            - correct_connected (bool): is node connected to correct terminal state?
            - current_cases list(PaseCase): Paths that lead to this state
        """
        self.state = ""
        self.rep_path = None
        self.rep_case = None
        self.children = {}
        self.parents = []
        self.terminated = False
        self.correct_connected = False
        # Beam search paths extraction
        self.current_cases = []


class SearchGraph(object):
    def __init__(self, example, config):
        """ A structure that holds all the example's discovered sequences
            root (StateNode): The Graph's initial state (represents the empty sequence)
            example (Example): The Graph's example
            config (Config): The search configurations
            nodes_table (dict{str: MonteCarloNode}): Maps a state's string representation to its node in the graph.
            correct_terminal_states (list[StateNode]): All the found states which represent correct programs.
            incorrect_terminal_states (list[StateNode]): All the found states which represent incorrect programs.
        """
        self.root = None
        self.example = example
        self.config = config
        self.nodes_table = {}
        self.correct_terminal_states = []
        self.incorrect_terminal_states = []

    def set_root_node(self, node):
        self.root = node
        self.root.rep_path = ParsePath.empty(self.example.context)
        self.root.rep_case = self.root.rep_path.extend()
        self.root.paths = [self.root.rep_path]

    def get_correct_beam_search(self, ranker, max_seq_length):
        """Extract the top-ranked correct programs from the graph with beam-search
        (in addition to incorrect programs that were found in the graph-search)
        Args:
            ranker: path ranking metric
            max_seq_length: number of beam-search iterations
        Returns:
            beam: found programs
        """
        terminated = []
        # If there are no correct paths just return the complete paths that were found
        if not len(self.correct_terminal_states):
            terminated.extend([node.rep_path for node in self.incorrect_terminal_states])
            terminated.sort(key=ranker, reverse=True)
            final_beam = Beam(terminated)
            return final_beam, []
        # Mark the correct paths
        self.mark_correct_seqs()
        self.root.current_cases = [self.root.rep_case]
        # Run beam-search over the marked sub-graph for correct paths extraction
        beam = [self.root]
        iterations = xrange(max_seq_length)
        for _ in iterations:
            beam = self.get_correct_advance(beam, terminated, ranker)
        terminated.extend([node.rep_path for node in self.incorrect_terminal_states])
        terminated.sort(key=ranker, reverse=True)
        final_beam = Beam(terminated)
        return final_beam, []

    def get_correct_advance(self, beam, terminated, ranker):
        """Advance beam.
        Args:
            beam: a beam
            terminated: list of found terminated programs
            ranker: the ranking metric
        Returns:
            beam: the next beam
        """
        next_prefixes = []
        prefix_to_node = {}
        for node in beam:
            for choice, (child, log_prob) in node.children.iteritems():
                if not child.correct_connected:
                    continue
                for case in node.current_cases:
                    expansion = case.copy_with_score(node.rep_case, choice)
                    if expansion.path.terminated:
                        assert check_denotation(self.example.answer, expansion.path.finalized_denotation)
                        terminated.append(expansion.path)
                        continue
                    else:
                        next_prefixes.append(expansion.path)
                        prefix_to_node[str(expansion)] = child
            node.current_cases = []
        # Sort the paths
        next_prefixes.sort(key=ranker, reverse=True)
        # Prune to beam size with exploration
        selected = next_prefixes[:self.config.correct_beam_size]
        # Create a beam from the remaining paths
        new_nodes = []
        for path in selected:
            new_node = prefix_to_node[str(path[-1])]
            new_node.current_cases.append(path.extend())
            new_nodes.append(new_node)
        new_nodes = list(set(new_nodes))
        return new_nodes

    def mark_correct_seqs(self):
        """mark the states that are connected to correct terminal states.
        """
        for node in self.correct_terminal_states:
            node.correct_connected = True
        queue = deque(self.correct_terminal_states)
        while queue:
            child = queue.popleft()
            for parent in child.parents:
                if parent.correct_connected:
                    continue
                parent.correct_connected = True
                queue.append(parent)

    def get_node(self, case):
        state = self.case_to_state(case)
        if state not in self.nodes_table:
            return None
        return self.nodes_table[state]

    def set_node(self, case, node):
        state = self.case_to_state(case)
        node.state = state
        self.nodes_table[state] = node

    def case_to_state(self, case):
        case_state = str((case.current_utterance, case.denotation, self.example.answer))
        return case_state


################################
# Value-based Beam-Search in Execution Space (VBSIX)


class BeamSearchNode(StateNode):
    def __init__(self):
        """ Contains all the value based beam-search related information.
            - prob (float) : the state's likelihood according to the model
            - current_prob (float) : the model's probability of reaching this state after t steps
            - previous_prob (float) : the model's probability of reaching this state after (t-1) steps
            - value (float) : the state's expected reward (estimated by the value function)
            - expanded (bool) : was this state already discovered and expanded (didn't fell out of the beam)?
        """
        super(BeamSearchNode, self).__init__()
        self.current_prob = 0
        self.previous_prob = 0
        self.value = -1
        self.expanded = False


class BeamSearchGraph(SearchGraph):
    def __init__(self, example, config):
        """ Extends the execution graph structure by maintaining a beam for beam-search:
            - current_beam(list(BeamSearchNode)) : a beam of states reachable in t steps
            - previous_beam(list(BeamSearchNode)) : a beam of states reachable in (t-1) steps
        """
        super(BeamSearchGraph, self).__init__(example, config)
        self.set_root_node(BeamSearchNode())
        self.current_beam = [self.root]
        self.next_beam = []

    def set_root_node(self, node):
        super(BeamSearchGraph, self).set_root_node(node)
        self.root.current_prob = 1
        self.root.previous_prob = 1

    def node_ranker(self, node):
        return node.current_prob

    def node_actor_critic_ranker(self, node):
        """ An additive interpolation of the actor and critic scores
        """
        if node.value == -1:
            value_weight = 0
        else:
            value_weight = self.config.value_weight
        prob = node.current_prob
        value = node.value
        rank = (value_weight * value) + (1 - value_weight) * prob
        return rank


class GraphBeamSearch(ExplorationPolicy):
    """ Runs a value-based beam-search in execution space, and extracts found programs.
    """
    def __init__(self, decoder, config, normalization, train):
        super(GraphBeamSearch, self).__init__(
            decoder, config, normalization, train)
        assert train

    def get_beams(self, examples, verbose=False):
        return self.get_intermediate_beams(examples, verbose)[0]

    def get_intermediate_beams(self, examples, verbose=False):
        # Build a search-graph for each example
        max_seq_length = get_num_iterations(
            self._config.iterations_per_utterance, examples)
        graphs = [BeamSearchGraph(ex, self._config) for ex in examples]
        root_cases = [graph.root.rep_case for graph in graphs]
        ignore_previous_utterances = \
            self._config.independent_utterance_exploration

        self._decoder.parse_model.score(root_cases,
                                        [],
                                        ignore_previous_utterances,
                                        self._decoder.caching)
        # Beam search over the execution space
        if verbose:
            iterations = verboserate(range(max_seq_length),
                                     desc='Performing beam search')
        else:
            iterations = xrange(max_seq_length)
        for i in iterations:
            self.advance(graphs, (i == max_seq_length - 1))

        # Extract beams from the search-graph
        all_beams = [graph.get_correct_beam_search(self._ranker, max_seq_length) for graph in graphs]
        beams = [beam for beam, intermediate in all_beams]
        intermediate_beams = [intermediate for beam, intermediate in all_beams]
        return beams, intermediate_beams

    def check_expansion(self, expansion):
        """ Checks expansion's validity
        """
        if isinstance(expansion.denotation, Exception):
            return False, False
        terminated = expansion.path.terminated
        if terminated and not expansion.path.finalizable:
            return False, terminated
        if not terminated and not self._decoder.path_checker(expansion.path):
            return False, terminated
        return True, terminated

    def advance(self, graphs, last_iter):
        """Advance a batch of beams.

        Args:
            graphs (list[BeamSearchGraph]): a batch of graphs for expansion
        """
        for graph in graphs:
            beam = graph.current_beam
            for node in beam:
                # expand node
                if not node.expanded:
                    case = node.rep_case
                    for choice, log_prob in zip(case.choices, case.choice_log_probs):
                        expansion = case.copy_with_decision(choice)
                        expansion_valid, expansion_terminated = self.check_expansion(expansion)
                        if not expansion_valid:
                            continue
                        child = graph.get_node(expansion)
                        # create a new child if its not already in the graph
                        if child is None:
                            child = BeamSearchNode()
                            child.rep_path = expansion.path
                            graph.set_node(expansion, child)
                            if expansion_terminated:
                                child.terminated = True
                                if check_denotation(graph.example.answer, expansion.path.finalized_denotation):
                                    child.value = 1
                                    graph.correct_terminal_states.append(child)
                                else:
                                    child.value = 0
                                    graph.incorrect_terminal_states.append(child)
                            else:
                                child.rep_case = expansion.path.extend()
                        # connect the node to its child
                        node.children[choice] = (child, log_prob)
                        child.parents.append(node)
                        child.current_prob = node.previous_prob * np.exp(log_prob) + child.current_prob
                        if not expansion_terminated:
                            graph.next_beam.append(child)
                    node.expanded = True
                else:
                    for _, (child, log_prob) in node.children.items():
                        if not child.terminated:
                            child.current_prob = node.previous_prob * np.exp(log_prob) + child.current_prob
                            graph.next_beam.append(child)
        if last_iter:
            return
        # Select the next beam
        epsilon = self._config.exploration_epsilon
        use_value = (self._config.value_weight > 0) and (self._decoder.step > self._config.value_ranking_start_step)
        cases_to_be_scored = []
        cases_to_be_scored_ans = []
        graphs_selected_nodes = []
        graphs_rerank = []
        eval_nodes = []
        eval_cases = []
        eval_ans = []
        # Select a set of continuations
        for graph in graphs:
            graph.next_beam = list(set(graph.next_beam))
            graph.next_beam.sort(key=graph.node_ranker, reverse=True)
            if use_value:
                # If value function is enabled, save nodes for evaluation and second selection
                selected = graph.next_beam[:self._config.first_selection_size]
                graphs_selected_nodes.append(selected)
                new_nodes = [node for node in selected if not node.expanded]
                eval_nodes_ex = [node for node in new_nodes
                                 if node.rep_case.current_utterance_idx >= (len(node.rep_case.context.utterances) - 2)]
                rerank_nodes = [node for node in selected
                                if node.rep_case.current_utterance_idx >= (len(node.rep_case.context.utterances) - 2)]
                eval_nodes.extend(eval_nodes_ex)
                eval_cases.extend([node.rep_case for node in eval_nodes_ex])
                eval_ans.extend([graph.example.answer] * len(eval_cases))
                graphs_rerank.append((len(rerank_nodes) > 0))
            else:
                # Else, populate the beam
                graph.current_beam = epsilon_greedy_sample(
                    graph.next_beam,
                    min(self._config.beam_size, len(graph.next_beam)),
                    epsilon=epsilon)
                new_nodes = [node for node in graph.current_beam if not node.expanded]
                num_cases_to_be_scored = len(new_nodes)
                cases_to_be_scored.extend([node.rep_case for node in new_nodes])
                if graph.current_beam:
                    while num_cases_to_be_scored < self._config.beam_size:
                        case = ParseCase.initial(graph.current_beam[0].rep_path.context)
                        cases_to_be_scored.append(case)
                        num_cases_to_be_scored += 1
                    cases_to_be_scored_ans.extend([graph.example.answer] * self._config.beam_size)
                for node in graph.next_beam:
                    node.previous_prob = node.current_prob
                    node.current_prob = 0
                graph.next_beam = []
        # If value function is enabled, populate the next beam according to the actor-critic ranker
        if use_value:
            if len(eval_cases):
                values = self._decoder.value_function.values(eval_cases, eval_ans)
                for node, value in zip(eval_nodes, values):
                    node.value = value
            for graph, selected_nodes, rerank in zip(graphs, graphs_selected_nodes, graphs_rerank):
                # Re-rank the nodes and select
                if rerank:
                    graph.next_beam[:self._config.first_selection_size] = \
                        sorted(selected_nodes, key=graph.node_actor_critic_ranker, reverse=True)
                graph.current_beam = epsilon_greedy_sample(
                    graph.next_beam,
                    min(self._config.beam_size, len(graph.next_beam)),
                    epsilon=epsilon)
                # Prepare nodes for decoding the next step
                new_nodes = [node for node in graph.current_beam if not node.expanded]
                cases_to_be_scored.extend([node.rep_case for node in new_nodes])
                num_cases_to_be_scored = len(new_nodes)
                if graph.current_beam:
                    while num_cases_to_be_scored < self._config.beam_size:
                        case = ParseCase.initial(graph.current_beam[0].rep_path.context)
                        cases_to_be_scored.append(case)
                        num_cases_to_be_scored += 1
                    cases_to_be_scored_ans.extend([graph.example.answer] * self._config.beam_size)
                for node in graph.next_beam:
                    node.previous_prob = node.current_prob
                    node.current_prob = 0
                graph.next_beam = []
        # Use the ParseModel to score
        ignore_previous_utterances = \
            self._config.independent_utterance_exploration
        self._decoder.parse_model.score(cases_to_be_scored,
                                        [],
                                        ignore_previous_utterances,
                                        self._decoder.caching)


def stack_to_str(exec_stack):
    str_elem = "["
    i = 0
    for elem in exec_stack:
        if isinstance(elem, list):
            if len(elem) == 1:
                str_elem = str_elem + str(elem[0])
            else:
                str_elem = str_elem + str(elem)
        else:
            str_elem = str_elem + str(elem)
        i = i + 1
        if i < len(exec_stack):
            str_elem = str_elem + ", "
    str_elem = str_elem + "]"
    return str_elem


def case_to_state(case):
    utter = case.current_utterance
    exec_stack = case._previous_cases[-1].denotation.execution_stack
    str_stack = stack_to_str(exec_stack)
    case_state = str(utter) + ";" + str_stack + ";" \
                 + str(case._previous_cases[-1].denotation.world_state.all_objects)
    if case.current_utterance_idx < len(case.context.utterances) - 1:
        case_state = case_state + ";" + str(case.context.utterances[case.current_utterance_idx + 1])
    return case_state


class ValueBeamSearch(ExplorationPolicy):
    """ Runs a value-based beam-search in program space (used for ablations test).
    """
    def __init__(self, decoder, config, normalization, train):
        super(ValueBeamSearch, self).__init__(
            decoder, config, normalization, train)
        assert train

    def get_beams(self, examples, verbose=False):
        beams = [Beam.initial_beam(ex.context) for ex in examples]
        num_iterations = get_num_iterations(
            self._config.iterations_per_utterance, examples)
        if verbose:
            iterations = verboserate(range(num_iterations),
                                     desc='Performing beam search')
        else:
            iterations = xrange(num_iterations)
        for _ in iterations:
            beams = self.advance(beams, examples)
        return beams

    def get_intermediate_beams(self, examples, verbose=False):
        beams = [Beam.initial_beam(ex.context) for ex in examples]
        intermediates = [[] for _ in examples]
        num_iterations = get_num_iterations(
            self._config.iterations_per_utterance, examples)
        if verbose:
            iterations = verboserate(range(num_iterations),
                                     desc='Performing beam search')
        else:
            iterations = xrange(num_iterations)
        for _ in iterations:
            for ex_idx, beam in enumerate(beams):
                intermediates[ex_idx].append(beam)
            beams = self.advance(beams, examples)
        return beams, intermediates

    def evaluate_cases(self, cases, answers):
        """ Uses the value network to evaluate a batch of states.
        Args:
            cases(list[ParseCases]): a list of parse-states to evaluate
            answers(list[Value]): the target denotations of the received states.
        Returns:
            list[float]: The states' expected rewards estimations
        """
        if len(cases) == 0:
            return []
        eval_states = []
        eval_cases = []
        eval_answers = []
        values = [-1] * len(cases) # -1: path that wasn't evaluated
        # Value network is only used on advanced on utterances' states and only from a specific training step
        if self._decoder.step < self._config.value_ranking_start_step:
            return values
        else:
            idx = [i for i, case in enumerate(cases)
                   if case.current_utterance_idx >= (len(case.context.utterances) - 2)]
        if len(idx) == 0:
            return values
        # Remove duplicate states for efficiency
        advanced_cases = np.array(cases)[idx]
        advanced_cases_ans = np.array(answers)[idx]
        for i, case, answer in zip(idx, advanced_cases, advanced_cases_ans):
            case_state = case_to_state(case)
            if case_state not in eval_states:
                eval_states.append(case_state)
                eval_cases.append(case)
                eval_answers.append(answer)
        # Run the value network
        eval_values = self._decoder.value_function.values(eval_cases, eval_answers)
        # Match each state to its value
        state_to_value = dict(zip(eval_states, eval_values))
        for i, advance_case in zip(idx, advanced_cases):
            state = case_to_state(advance_case)
            values[i] = state_to_value[state]
        return values

    def advance(self, beams, examples):
        """Advance a batch of beams.

        Args:
            beams (list[Beam]): a batch of beams

        Returns:
            list[Beam]: a new batch of beams
                (in the same order as the input beams)
        """
        # Gather everything needed to be scored
        # For efficiency, pad so that the number of cases from each beam
        #   is equal to beam_size.
        cases_to_be_scored = []
        cases_to_be_scored_ans = []
        new_paths = []
        for beam, example in zip(beams, examples):
            # terminated stores terminated paths
            # which do not count toward the beam size limit
            terminated = []
            # unterminated stores unterminated paths and a partial ParseCase
            # containing the possible candidate choices
            unterminated = []
            num_cases_to_be_scored = 0
            # print '@' * 40
            for path in beam:
                if path.terminated:
                    terminated.append(path)
                else:
                    case = path.extend()
                    unterminated.append((path, case))
                    cases_to_be_scored.append(case)
                    cases_to_be_scored_ans.append(example.answer)
                    num_cases_to_be_scored += 1
            new_paths.append((terminated, unterminated))
            # Pad to beam_size
            assert num_cases_to_be_scored <= self._config.beam_size

            if beam:
                while num_cases_to_be_scored < self._config.beam_size:
                    case = ParseCase.initial(beam[0].context)
                    cases_to_be_scored.append(case)
                    cases_to_be_scored_ans.append(example.answer)
                    num_cases_to_be_scored += 1
        # for exploration, use a parser which pretends like every utterance
        # is the first utterance it is seeing
        ignore_previous_utterances = \
            self._config.independent_utterance_exploration
        # Use the ParseModel to score
        self._decoder.parse_model.score(cases_to_be_scored,
                                        [],
                                        ignore_previous_utterances,
                                        self._decoder.caching)

        # Read the scores and create new paths
        new_beams = []
        new_unterminated = []
        for k, (new_paths_ex, example) in enumerate(zip(new_paths, examples)):
            terminated, unterminated = new_paths_ex
            new_unterminated_ex = []
            for path, case in unterminated:
                for choice in case.choices:
                    clone = case.copy_with_decision(choice)
                    denotation = clone.denotation
                    # Filter out the cases with invalid denotation
                    if not isinstance(denotation, Exception):
                        path = clone.path
                        if path.terminated:
                            try:
                                # Test if the denotation can be finalized
                                path.finalized_denotation
                                terminated.append(path)
                            except ValueError as e:
                                pass
                        elif self._decoder.path_checker(path):
                            new_unterminated_ex.append(path)
                        else:
                            pass
                    else:
                        pass
            new_unterminated.append(new_unterminated_ex)
        # First selection: select the most probable paths according to the policy
        eval_cases = []
        eval_ans = []
        selected_unterminated = []
        for example, new_unterminated_ex in zip(examples, new_unterminated):
            new_unterminated_ex.sort(key=self._ranker, reverse=True)
            selected = new_unterminated_ex[:self._config.first_selection_size]
            eval_cases.extend([path.extend() for path in selected])
            eval_ans.extend([example.answer] * len(selected))
            selected_unterminated.append(selected)
        values = self.evaluate_cases(eval_cases, eval_ans)
        top = 0
        # Second selection: build the next beam from selected path, according to the actor and the critic.
        for new_path_ex, new_unterminated_ex, sel_unterminated_ex in \
                zip(new_paths, new_unterminated, selected_unterminated):
            terminated, _ = new_path_ex
            terminated.sort(key=self._ranker, reverse=True)
            paths_values = []
            for path, value in zip(sel_unterminated_ex, values[top: top + len(sel_unterminated_ex)]):
                path_value = PathValue(path, value)
                paths_values.append(path_value)
            top = top + len(sel_unterminated_ex)
            # Sort the paths
            paths_values.sort(key=self._q_ranker, reverse=True)
            reranked_paths = [pv.path for pv in paths_values]
            new_unterminated_ex[:self._config.first_selection_size] = reranked_paths
            # Prune to beam size with exploration
            epsilon = self._config.exploration_epsilon
            selected_paths = epsilon_greedy_sample(
                new_unterminated_ex,
                min(self._config.beam_size, len(paths_values)),
                epsilon=epsilon)
            # Create a beam from the remaining paths
            new_beams.append(Beam(terminated + selected_paths))
        return new_beams


################################
# Beam search


class BeamSearchExplorationPolicy(ExplorationPolicy):
    def __init__(self, decoder, config, normalization, train):
        super(BeamSearchExplorationPolicy, self).__init__(
            decoder, config, normalization, train)
        if not train:
            assert not config.independent_utterance_exploration
            assert config.exploration_epsilon == 0

    def get_beams(self, examples, verbose=False):
        beams = [Beam.initial_beam(ex.context) for ex in examples]
        answers = [ex.answer for ex in examples]
        num_iterations = get_num_iterations(
            self._config.iterations_per_utterance, examples)
        if verbose:
            iterations = verboserate(range(num_iterations),
                                     desc='Performing beam search')
        else:
            iterations = xrange(num_iterations)
        for _ in iterations:
            beams = self.advance(beams, answers)
        return beams

    def get_intermediate_beams(self, examples, verbose=False):
        beams = [Beam.initial_beam(ex.context) for ex in examples]
        answers = [ex.answer for ex in examples]
        intermediates = [[] for _ in examples]
        num_iterations = get_num_iterations(
            self._config.iterations_per_utterance, examples)
        if verbose:
            iterations = verboserate(range(num_iterations),
                                     desc='Performing beam search')
        else:
            iterations = xrange(num_iterations)
        for _ in iterations:
            for ex_idx, beam in enumerate(beams):
                intermediates[ex_idx].append(beam)
            beams = self.advance(beams, answers)
        for beam, example in zip(beams, examples):
            cnt = 0
            total = 0
            #print("========")
            #print(example.context.world)
            #print(example.context.utterances)
            #print(example.answer)
            #print("")
            #print("")
            for path in beam:
                if path.terminated:
                    total = total + 1
                    if check_denotation(example.answer, path.finalized_denotation):
                        #print("{}. {}, {}".format(cnt, path[-1], np.exp(path.log_prob)))
                        #for case in path:
                        #    print("     case: {}, prob: {}".format(case, np.exp(case.log_prob)))
                        cnt = cnt + 1
            #print("beam: total: {}, correct: {} ".format(total, cnt))
        return beams, intermediates

    def advance(self, beams, answers):
        """Advance a batch of beams.

        Args:
            beams (list[Beam]): a batch of beams

        Returns:
            list[Beam]: a new batch of beams
                (in the same order as the input beams)
        """
        # Gather everything needed to be scored
        # For efficiency, pad so that the number of cases from each beam
        #   is equal to beam_size.
        cases_to_be_scored = []
        cases_answers = []
        new_paths = []
        for beam, answer in zip(beams, answers):
            # terminated stores terminated paths
            # which do not count toward the beam size limit
            terminated = []
            # unterminated stores unterminated paths and a partial ParseCase
            # containing the possible candidate choices
            unterminated = []
            num_cases_to_be_scored = 0
            # print '@' * 40
            for path in beam:
                if path.terminated:
                    terminated.append(path)
                else:
                    case = path.extend()
                    unterminated.append((path, case))
                    cases_to_be_scored.append(case)
                    cases_answers.append(answer)
                    num_cases_to_be_scored += 1
            new_paths.append((terminated, unterminated))
            # Pad to beam_size
            assert num_cases_to_be_scored <= self._config.beam_size

            if beam:
                while num_cases_to_be_scored < self._config.beam_size:
                    case = ParseCase.initial(beam[0].context)
                    cases_to_be_scored.append(case)
                    cases_answers.append(answer)
                    num_cases_to_be_scored += 1

        # for exploration, use a parser which pretends like every utterance
        # is the first utterance it is seeing
        ignore_previous_utterances = \
            self._config.independent_utterance_exploration
        # Use the ParseModel to score
        self._decoder.parse_model.score(cases_to_be_scored, [],
                                        ignore_previous_utterances,
                                        self._decoder.caching)
        # Read the scores and create new paths
        new_beams = []
        # print '=' * 40
        for terminated, unterminated in new_paths:
            # print '-' * 20
            new_unterminated = []
            for path, case in unterminated:
                cnt = 0
                for choice in case.choices:
                    clone = case.copy_with_decision(choice)
                    denotation = clone.denotation
                    # Filter out the cases with invalid denotation
                    if not isinstance(denotation, Exception):
                        path = clone.path
                        if path.terminated:
                            try:
                                # Test if the denotation can be finalized
                                path.finalized_denotation
                                # print 'FOUND [T]', clone.path.decisions, denotation, denotation.utterance_idx, path.finalized_denotation
                                terminated.append(path)
                                cnt = cnt + 1
                            except ValueError as e:
                                # print 'FOUND [BAD T]', e
                                pass
                        elif self._decoder.path_checker(path):
                            # print 'FOUND', clone.path.decisions, denotation, denotation.utterance_idx
                            cnt = cnt + 1
                            new_unterminated.append(path)
                        else:
                            # print 'PRUNED', clone.path.decisions, denotation, denotation.utterance_idx
                            pass
                    else:
                        # print 'BAD', clone.path.decisions, denotation
                        pass
            # Sort the paths
            terminated.sort(key=self._ranker, reverse=True)
            new_unterminated.sort(key=self._ranker, reverse=True)
            # Prune to beam size with exploration
            epsilon = self._config.exploration_epsilon
            selected = epsilon_greedy_sample(
                new_unterminated,
                min(self._config.beam_size, len(new_unterminated)),
                epsilon=epsilon)
            # Create a beam from the remaining paths
            new_beams.append(Beam(terminated + selected))
        return new_beams

################################
# Stale Beam Search

class BeamMetaInfo(object):
    """Wrapper around a Beam that includes metadata for BeamMap"""

    def __init__(self, beam, age):
        self._beam = beam
        self._age = age

    @property
    def beam(self):
        return self._beam

    @property
    def age(self):
        return self._age

    def increment_age(self):
        self._age += 1


class BeamMap(object):
    """Maintains a map between Example and stale Beams"""

    def __init__(self):
        self._map = {}  # example --> BeamMetaInfo

    def contains(self, example):
        """Returns if example is in the map

        Args:
            example (Example)

        Returns:
            bool: True if example in map
        """
        return example in self._map

    def get_beam_age(self, example):
        """Returns how old the beam for this example is

        Args:
            example (Example)

        Returns:
            int: the age
        """
        assert self.contains(example)

        return self._map[example].age

    def increment_age(self, example):
        """Increments the age of the beam associated with this example

        Args:
            example (Example)
        """
        assert example in self._map
        self._map[example].increment_age()

    def set_beam(self, example, beam):
        """Sets the beam associated with this example.

        Args:
            example (Example)
            beam (Beam)
        """
        self._map[example] = BeamMetaInfo(beam, 1)

    def get_beam(self, example):
        """Returns the beam associated with this example.

        Args:
            example (Example)

        Returns:
            Beam
        """
        assert example in self._map
        return self._map[example].beam


class StaleBeamSearch(ExplorationPolicy):
    """Performs beam search every max_age iterations.
    On the other iterations, returns the stale beams.
    NOTE: Does not recalculate scores

    Args:
        decoder (Decoder)
        config (Config)
        normalization (NormalizationOptions)
        fresh_policy (ExplorationPolicy): the policy that runs to obtain
            fresh beams
        train (bool): train or test policy
    """

    def __init__(self, decoder, config, normalization, train):
        if not train:
            raise ValueError(
                "Stale Beam Search should only be used at train time")
        super(StaleBeamSearch, self).__init__(
            decoder, config, normalization, train)
        self._fresh_policy = get_exploration_policy(
            decoder, config.fresh_policy, normalization, train)
        self._max_age = self._config.max_age  # iterations till refresh
        self._beam_map = BeamMap()

    def get_beams(self, examples, verbose=False):
        expired_examples = []  # Needs to be updated with BeamSearch
        fresh_beams = []  # Fetched from cache
        fresh_indices = []  # True @i if example i is fresh
        for example in examples:
            # Non-existent or expired
            if not self._beam_map.contains(example) or \
                            self._beam_map.get_beam_age(example) >= self._max_age:
                fresh_indices.append(False)
                expired_examples.append(example)
            else:  # Still fresh
                self._beam_map.increment_age(example)
                fresh_indices.append(True)
                fresh_beams.append(self._beam_map.get_beam(example))

        # Recalculate expired beams
        if len(expired_examples) > 0:
            recalculated_beams = self._fresh_policy.get_beams(
                expired_examples, verbose)
        else:
            recalculated_beams = []

        # Cache recalculated beams
        for expired_example, recalculated_beam in zip(
                expired_examples, recalculated_beams):
            self._beam_map.set_beam(expired_example, recalculated_beam)

        # Put beams back in correct order
        beams = []
        for fresh in fresh_indices:
            if fresh:
                beams.append(fresh_beams.pop(0))
            else:
                beams.append(recalculated_beams.pop(0))

        return beams

    def get_intermediate_beams(self, examples, verbose=False):
        return self._fresh_policy.get_intermediate_beams(
            examples, verbose=verbose)


################################
# Gamma Sampling ABC

class GammaSamplingExplorationPolicy(ExplorationPolicy):
    """Creates a beam using some form of sampling."""
    __metaclass__ = ABCMeta

    def __init__(self, decoder, config, normalization, train):
        if not train:
            raise ValueError(
                "Sampling Exploration should only be used at train time.")
        super(GammaSamplingExplorationPolicy, self).__init__(
            decoder, config, normalization, train)
        assert config.exploration_epsilon is None

    def get_beams(self, examples, verbose=False):
        terminated = [set() for _ in examples]
        # initialize beams
        beams = [[ParsePath.empty(ex.context)] for ex in examples]
        # put all probability mass on the root
        distributions = [[1] for _ in examples]
        num_iterations = get_num_iterations(
            self._config.iterations_per_utterance, examples)

        iterations = xrange(num_iterations)
        if verbose:
            iterations = verboserate(
                iterations, desc='Performing randomized search')

        for _ in iterations:
            terminated, beams, distributions = self.advance(
                terminated, beams, distributions)

        return [Beam(sorted(list(paths), key=self._ranker, reverse=True))
                for paths in terminated]

    def get_intermediate_beams(self, examples, verbose=False):
        intermediates = [[] for _ in examples]

        terminated = [set() for ex in examples]
        particles = [[ParsePath.empty(ex.context)] for ex in examples]
        distributions = [[1] for _ in xrange(len(examples))]
        num_iterations = get_num_iterations(
            self._config.iterations_per_utterance, examples)

        if verbose:
            iterations = verboserate(range(num_iterations),
                                     desc='Performing randomized search')
        else:
            iterations = xrange(num_iterations)

        for _ in iterations:
            for ex_idx, (beam, terminated_set) in enumerate(
                    zip(particles, terminated)):
                intermediates[ex_idx].append(Beam(
                    sorted(terminated_set, key=self._ranker, reverse=True) +
                    sorted(beam, key=self._ranker, reverse=True)))
            terminated, particles, distributions = self.advance(
                terminated, particles, distributions)

        return [Beam(sorted(list(paths), key=self._ranker, reverse=True))
                for paths in terminated], intermediates

    def advance(self, terminated, beams, empirical_distributions):
        """Advance a batch of beams.

        Args:
            terminated (list[set(ParsePath)]): a batch of all the
                terminated paths found so far for each beam.
            beams (list[list[ParsePath]]): a batch of beams.
                All paths on all beams have the same length (all
                should be unterminated)
            empirical_distributions (list[list[float]]): a batch of
                distributions over the corresponding beams.

        Returns:
            list[set[ParsePath]]: a batch of terminated beams
                (in the same order as the input beams)
            list[list[ParsePath]]: a batch of new beams all extended
                by one time step
            list[list[float]]: the new empirical distributions over these
                particles
        """
        # nothing on the beams should be terminated
        # terminated paths should be in the terminated set
        for beam in beams:
            for path in beam:
                assert not path.terminated

        path_extensions = [[path.extend() for path in beam] for beam in beams]

        # for exploration, use a parser which pretends like every utterance
        # is the first utterance it is seeing
        ignore_previous_utterances = \
            self._config.independent_utterance_exploration

        # Use the ParseModel to score
        self._decoder.parse_model.score(flatten(path_extensions),
                                        ignore_previous_utterances,
                                        self._decoder.caching)

        new_beams = []
        new_distributions = []
        gamma = self._config.exploration_gamma
        for terminated_set, cases, distribution in zip(
                terminated, path_extensions, empirical_distributions):

            new_path_log_probs = []
            paths_to_sample_from = []

            for case, path_prob in zip(cases, distribution):
                for continuation in case.valid_continuations(
                        self._decoder.path_checker):
                    # Add all the terminated paths
                    if continuation.terminated:
                        terminated_set.add(continuation)
                    else:
                        # Sample from unterminated paths
                        new_path_log_probs.append(
                            gamma * continuation[-1].log_prob +
                            np.log(path_prob))
                        paths_to_sample_from.append(continuation)

            if len(paths_to_sample_from) == 0:
                new_beams.append([])
                new_distributions.append([])
                continue

            new_path_probs = softmax(new_path_log_probs)

            new_particles, new_distribution = self._sample(
                paths_to_sample_from, new_path_probs)
            new_beams.append(new_particles)
            new_distributions.append(new_distribution)

        return terminated, new_beams, new_distributions

    @abstractmethod
    def _sample(self, paths_to_sample_from, path_probs):
        """Sample from set of valid paths to sample from according to policy.

        Args:
            paths_to_sample_from (list[ParsePath]): the valid paths in
                next beam
            path_probs (list[float]): gamma sharpened probs of each path

        Returns:
            list[ParsePath]: the paths that are sampled according to
                this policy
            list[float]: the new probabilities associated with these paths
                for the next iteration
        """
        raise NotImplementedError


################################
# Particle filter

class ParticleFiltering(GammaSamplingExplorationPolicy):
    """Estimates an empirical distribution from gamma-sharpened distribution
    given by ParseModel. Samples from that empirical distribution.

    1. Sample from empirical distribution p_hat (until get beam_size unique)
    2. Extend particles using true distribution

    Args:
        decoder (Decoder)
        config (Config)
        normalization (NormalizationOptions)
    """

    def _sample(self, paths_to_sample_from, path_probs):
        # Samples without replacement. New particles have empirical
        # distribution according to their frequency.
        num_to_sample = min(
            self._config.beam_size, len(paths_to_sample_from))
        sampled_particles = sample_with_replacement(
            paths_to_sample_from, path_probs, num_to_sample)
        new_particle_counts = Counter(sampled_particles)
        new_particles = new_particle_counts.keys()
        new_distribution = np.array(new_particle_counts.values())
        new_distribution = list(
            new_distribution / float(np.sum(new_distribution)))
        return new_particles, new_distribution


################################
# Gamma Randomized Search

class GammaRandomizedSearch(GammaSamplingExplorationPolicy):
    def _sample(self, paths_to_sample_from, path_probs):
        # Samples without replacement
        num_to_sample = min(
            self._config.beam_size, len(paths_to_sample_from),
            sum(p > 0 for p in path_probs)
        )
        chosen_indices = np.random.choice(
            xrange(len(paths_to_sample_from)), size=num_to_sample,
            replace=False, p=path_probs)
        new_particles = [
            paths_to_sample_from[index] for index in chosen_indices]

        # Distribution is just gamma sharpened and normalized path probs
        new_distribution = softmax(
            [self._config.exploration_gamma * path.log_prob
             for path in new_particles])
        return new_particles, new_distribution


################################
# Batched REINFORCE

class BatchedReinforce(ExplorationPolicy):
    """Exploration policy that sample K independent paths for each example
    (where K = beam size).

    - The paths comes from the model distribution p(z|x) with possible modifications
      using gamma or epsilon.
    - Specifically the next predicate is sampled from
        * gamma-softmaxed p(choice) with probability 1 - epsilon
        * uniform over choices      with probability epsilon
    - Choices that cannot be executed are not considered.
    - Paths that cannot be extended are discarded by default.
        * Turn on "zombie_mode" to keep them on the beam for negative update
    - There are two ways to handle terminated paths:
        * Default: The last predicate must be sampled like other predicates
        * termination_lookahead: For any choice that terminates the path,
            apply it and add the terminated path to the beam.
            Still keep extending the original path.

    Possible configs:
    - beam_size (int)
    - independent_utterance_exploration (bool)
    - exploration_gamma (float)
    - exploration_epsilon (float)
    - termination_lookahead (bool)
    - zombie_mode (bool)
    """

    def __init__(self, decoder, config, normalization, train):
        if not train:
            raise ValueError(
                "Batched REINFORCE should only be used at train time")
        super(BatchedReinforce, self).__init__(
            decoder, config, normalization, train)

    def get_beams(self, examples, verbose=False):
        return self.get_intermediate_beams(examples, verbose)[0]

    def get_intermediate_beams(self, examples, verbose=False):
        # Start with beam_size empty paths for each example
        beams = [Beam([ParsePath.empty(ex.context)
                       for _ in xrange(self._config.beam_size)])
                 for ex in examples]
        intermediates = [[] for _ in examples]
        num_iterations = get_num_iterations(
            self._config.iterations_per_utterance, examples)
        if verbose:
            iterations = verboserate(range(num_iterations),
                                     desc='Batched REINFORCE')
        else:
            iterations = xrange(num_iterations)
        for _ in iterations:
            for ex_idx, beam in enumerate(beams):
                intermediates[ex_idx].append(beam)
            beams = self.advance(beams)
        return beams, intermediates

    def advance(self, beams):
        """Advance a batch of beams.

        Args:
            beams (list[Beam]): a batch of beams

        Returns:
            list[Beam]: a new batch of beams
                (in the same order as the input beams)
        """
        # Extend a new case for each unterminated path
        cases_to_be_scored = []
        extending = []
        for beam in beams:
            terminated, unterminated = [], []
            for path in beam:
                if path.terminated:
                    terminated.append(path)
                else:
                    case = path.extend()
                    cases_to_be_scored.append(case)
                    unterminated.append((path, case))
            extending.append((terminated, unterminated))
        # Score them
        ignore_previous_utterances = \
            self._config.independent_utterance_exploration
        self._decoder.parse_model.score(
            cases_to_be_scored, ignore_previous_utterances, False)
        # Read the scores and create new paths
        all_new_beams = []
        for new_beam, unterminated in extending:
            for old_path, case in unterminated:
                valid_choice_indices = []
                valid_new_paths = []
                for index, choice in enumerate(case.choices):
                    clone = case.copy_with_decision(choice)
                    denotation = clone.denotation
                    # Filter out the cases with invalid denotation
                    if not isinstance(denotation, Exception):
                        new_path = clone.path
                        # Filter out invalid paths
                        if new_path.terminated:
                            if new_path.finalizable:
                                # With termination_lookahead, add it to beam
                                if self._config.termination_lookahead:
                                    new_beam.append(new_path)
                                else:
                                    valid_choice_indices.append(index)
                                    valid_new_paths.append(new_path)
                        elif self._decoder.path_checker(new_path):
                            valid_choice_indices.append(index)
                            valid_new_paths.append(new_path)
                if valid_choice_indices:
                    # Sample a choice
                    epsilon = self._config.exploration_epsilon
                    gamma = self._config.exploration_gamma
                    if np.random.random() > epsilon:
                        probs = softmax([case.choice_logits[i] * gamma
                                         for i in valid_choice_indices])
                    else:
                        probs = ([1. / len(valid_choice_indices)]
                                 * len(valid_choice_indices))
                    selected_index = np.random.choice(
                        range(len(valid_new_paths)), p=probs)
                    new_beam.append(valid_new_paths[selected_index])
                elif self._config.zombie_mode and len(old_path):
                    # Make a zombie copy of the last previous ParseCase
                    new_beam.append(old_path.zombie_clone())
            all_new_beams.append(Beam(new_beam))
        return all_new_beams


################################
# Main method

def get_exploration_policy(decoder, config, normalization, train):
    """Returns the ExplorationPolicy corresponding to the
    config.exploration_policy entry.

    Args:
        decoder (Decoder): The Decoder
        config (Config): Should be the config specified in the Decoder
        normalization (NormalizationOptions): The normalization
        train (bool): Whether the policy should be train or test

    Returns:
        ExplorationPolicy
    """
    if config.type == "beam-search":
        return BeamSearchExplorationPolicy(decoder, config, normalization, train)
    elif config.type == "value-beam-search":
        return ValueBeamSearch(decoder, config, normalization, train)
    elif config.type == "graph-beam-search":
        return GraphBeamSearch(decoder, config, normalization, train)
    elif config.type == "monte-carlo-tree-search":
        return MonteCarloTreeSearch(decoder, config, normalization, train)
    elif config.type == "particle-filtering":
        return ParticleFiltering(decoder, config, normalization, train)
    elif config.type == "gamma-randomized-search":
        return GammaRandomizedSearch(decoder, config, normalization, train)
    elif config.type == "stale-beam-search":
        return StaleBeamSearch(decoder, config, normalization, train)
    elif config.type == "batched-reinforce":
        return BatchedReinforce(decoder, config, normalization, train)
    else:
        raise ValueError(
            "{} does not specify a valid ExplorationPolicy".format(
                config.type))