import abc
import random
import sys
from collections import namedtuple

import numpy as np
import tensorflow as tf

from gtd.ml.framework import Feedable, Model
from keras.layers import Dense

from gtd.ml.model import Attention, TokenEmbedder
from gtd.utils import flatten
from strongsup.embeddings import RLongPrimitiveEmbeddings
from strongsup.parse_case import PrettyCaseEmbedding
from strongsup.parse_model import RLongObjectEmbedder
from strongsup.utils import OptimizerOptions, get_optimizer
from strongsup.value import check_denotation


class ValueFunctionExample(namedtuple('ValueFunctionExample', ['case', 'answer', 'reward'])):
    """Represents a single training example for StateValueFunction.train_step.

    Attributes:
        case (ParseCase)
        answer (list[Value])
        reward (float): typically 0 or 1
    """
    __slots__ = ()

    @classmethod
    def examples_from_paths(cls, paths, example):
        """Return a list of ValueFunctionExamples derived from ParsePaths discovered during exploration.

        Args:
            paths (list[ParsePath])
            example (strongsup.example.Example)

        Returns:
            list[ValueFunctionExample]
        """
        vf_examples = []
        for path in paths:
            reward = 1 if check_denotation(example.answer, path.finalized_denotation) else 0
            vf_examples.extend(ValueFunctionExample(case, example.answer, reward) for case in path)
        return vf_examples

    @staticmethod
    def stack_to_str(exec_stack):
        str_elem = "["
        i = 0
        for elem in exec_stack:
            if isinstance(elem, list):
                if len(elem) == 1:
                    str_elem = str_elem + str(elem[0])
                else:
                    str_elem = str_elem + str(elem)
            else:
                str_elem = str_elem + str(elem)
            i = i + 1
            if i < len(exec_stack):
                str_elem = str_elem + ", "
        str_elem = str_elem + "]"
        return str_elem

    @classmethod
    def examples_from_paths_batch(cls, paths_batch, examples):
        """Return a list of ValueFunctionExamples derived from ParsePaths discovered during exploration.

        Args:
            paths_batch (list[list[ParsePath]])
            examples (list[strongsup.example.Example])
        Returns:
            Dict{Example: list[ValueFunctionExample]}
        """
        case_states = []
        parse_to_vf_ex = {}
        vf_examples = []
        for paths, example in zip(paths_batch, examples):
            case_to_reward = {}
            postfix_cache = {}
            # Compute an approximation of the true expected reward for each state encountered in the paths set
            for path in paths:
                reward = 1 if check_denotation(example.answer, path.finalized_denotation) else 0
                for j, case in enumerate(path):
                    # Extract parse state
                    if len(case._previous_cases) == 0:
                        continue
                    if case.current_utterance_idx < len(case.context.utterances) - 2:
                        continue
                    prev_case_path = case._previous_cases[-1].path
                    exec_stack = case._previous_cases[-1].denotation.execution_stack
                    str_stack = cls.stack_to_str(exec_stack)
                    utter = case.current_utterance
                    case_state = str(utter) + ";" + str_stack + ";" \
                                 + str(case._previous_cases[-1].denotation.world_state.all_objects)
                    if case.current_utterance_idx < len(case.context.utterances) - 1:
                        case_state = case_state + ";" + str(case.context.utterances[case.current_utterance_idx + 1])
                    # Add correct suffix probability to the state's expected reward, if it wasn't added already
                    if case_state not in case_to_reward:
                        case_to_reward[case_state] = 0
                    if case_state not in postfix_cache:
                        postfix_cache[case_state] = []
                    correct_mass = case_to_reward[case_state]
                    path_prob = np.exp(path.log_prob)
                    case_prob = np.exp(prev_case_path.log_prob)
                    post_actions = [post_case.decision for post_case in path[j:]]
                    if post_actions in postfix_cache[case_state]:
                        continue
                    if case_prob:
                        prob = float(path_prob) / float(case_prob)
                    else:
                        prob = 0
                    case_to_reward[case_state] = correct_mass + reward * prob
                    postfix_cache[case_state].append(post_actions)

            # Build value network examples
            for path in paths:
                for case in path:
                    if len(case._previous_cases) == 0:
                        continue
                    if case.current_utterance_idx < len(case.context.utterances) - 2:
                        continue
                    exec_stack = case._previous_cases[-1].denotation.execution_stack
                    utter = case.current_utterance
                    case_state = str(utter) + ";" + cls.stack_to_str(exec_stack) \
                                 + ";" + str(case._previous_cases[-1].denotation.world_state.all_objects)
                    if case.current_utterance_idx < len(case.context.utterances) - 1:
                        case_state = case_state + ";" + str(case.context.utterances[case.current_utterance_idx + 1])
                    if case_state in case_states:
                        continue
                    case_states.append(case_state)
                    label = case_to_reward[case_state]
                    if label > 1:
                        label = 1
                    if example not in parse_to_vf_ex:
                        parse_to_vf_ex[example] = []
                    parse_to_vf_ex[example].append(ValueFunctionExample(case, example.answer, label))
                    vf_examples.append(ValueFunctionExample(case, example.answer, label))
        return parse_to_vf_ex, vf_examples


class StateValueFunction(Model):
    __metaclass__ = abc.ABCMeta

    """Defines a value function that associates a value V to each state s as in RL"""

    @abc.abstractmethod
    def values(self, cases, answers):
        """Returns the values for the states corresponding to a list of cases
        in the same order.

        Args:
            cases (list[ParseCase]): the cases
            answers (list[list[Value]]): the target answer

        Returns:
            values (list[float]): the values in same order as cases
        """
        raise NotImplementedError

    @abc.abstractmethod
    def loss(self, vf_examples):
        """Compute the loss for which we are performing gradient descent upon.

        Args:
            vf_examples (list[ValueFunctionExample])

        Returns:
            float
        """
        raise NotImplementedError

    @abc.abstractmethod
    def train_step(self, vf_examples):
        """Takes a train step based on training examples

        Args:
            vf_examples (list[ValueFunctionExample])
        """
        raise NotImplementedError


class ConstantValueFunction(StateValueFunction):
    """Gives every state the same value"""

    def __init__(self, constant_value):
        self._constant_value = constant_value

    def values(self, cases, answers):
        return np.array([self._constant_value] * len(cases))

    @property
    def constant_value(self):
        return self._constant_value

    def loss(self, vf_examples):
        """Loss in terms of mean squared error."""
        if len(vf_examples) == 0:
            return 0.0

        c = self._constant_value
        diffs = [(c - ex.reward) for ex in vf_examples]
        return np.mean(np.power(diffs, 2))

    def train_step(self, vf_examples):
        """Is a no-op"""
        return


class LogisticValueFunction(StateValueFunction, Feedable):
    def __init__(self, parse_model, world_embeder, primitive_embed_dim, learning_rate, optimizer_opt):
        """
        Args:
            parse_model (ParseModel)
            learning_rate (float)
            optimizer_opt (OptimizerOptions)
        """
        with tf.name_scope("LogisticValueFunction"):
            self._rewards = tf.placeholder(
                tf.float32, shape=[None], name="rewards")
            encodings = parse_model.case_encodings
            parser_input = parse_model.input_layer
            world_embeds = world_embeder.embeds
            curr_world_embeds_flat = world_embeds[:, 0, :]
            answer_embeds_flat = world_embeds[:, 1, :]
            fut_embeds = parse_model._fut_embeds
            input_layer = tf.concat(1, [parser_input, fut_embeds, curr_world_embeds_flat, answer_embeds_flat])
            h = input_layer
            h_dims = [256, 256]
            for h_dim in h_dims:
                h = Dense(h_dim, activation='relu', bias=True)(h)  # (batch_size, h_dim)
            query = h
            self._values = tf.squeeze(
                Dense(1, activation="sigmoid", bias=True)(query),
                axis=[1])
        loss = tf.reduce_mean(tf.contrib.losses.log_loss(self._values, labels=self._rewards))
        optimizer = get_optimizer(optimizer_opt)(learning_rate)
        grads_and_vars = optimizer.compute_gradients(loss)
        grads, _ = list(zip(*grads_and_vars))
        self._norms = tf.global_norm(grads)
        self._take_step = optimizer.minimize(loss)
        self._parse_model = parse_model
        self._world_embedder = world_embeder
        self.fut_embeds = fut_embeds
        # Hold it around for testing purposes
        self._loss = loss
        self._encoding = encodings
        self._query = query
        self._input_layer = input_layer
        self._world_embeds = world_embeds
        self._answer_embeds_flat = answer_embeds_flat
        self._curr_world_embeds_flat = curr_world_embeds_flat

    @classmethod
    def _unpack_vf_examples(cls, vf_examples):
        cases = [ex.case for ex in vf_examples]
        answers = [ex.answer for ex in vf_examples]
        rewards = [ex.reward for ex in vf_examples]
        return cases, answers, rewards

    def values(self, cases, answers, ignore_previous_utterances=False):
        if len(cases) == 0:
            # Should only happen if everything gets pruned off beam.
            return []
        fetch = {"values": self._values}
        fetched = self.compute(
            fetch, cases, answers, rewards=None,
            ignore_previous_utterances=ignore_previous_utterances)
        return fetched["values"]

    def values_and_scores(self, cases, answers, ignore_previous_utterances=False):

        if len(cases) == 0:
            # Should only happen if everything gets pruned off beam.
            return []
        fetch = {
            'values': self._values,
            'logits': self._parse_model.logits,
            'log_probs': self._parse_model.log_probs,
            'stack_hash': self._parse_model.stack_embedder.embeds_hash
        }
        # fetch variables
        fetched = self.compute(
            fetch, cases, answers, rewards=None,
            ignore_previous_utterances=ignore_previous_utterances)

        # unpack fetched values
        logits, log_probs = fetched['logits'], fetched['log_probs']  # numpy arrays with shape (batch_size, max_choices)
        stack_hash = fetched['stack_hash']
        history_hash = [None] * len(cases)

        num_nans = lambda arr: np.sum(np.logical_not(np.isfinite(arr)))

        # cut to actual number of choices
        for i, case in enumerate(cases):
            case.choice_logits = logits[i, :len(case.choices)]
            case.choice_log_probs = log_probs[i, :len(case.choices)]
            case.pretty_embed = PrettyCaseEmbedding(history_hash[i], stack_hash[i])

            logit_nans = num_nans(case.choice_logits)
            log_prob_nans = num_nans(case.choice_log_probs)

            # Tracking NaN
            if logit_nans > 0:
                tf.logging.error("logit NaNs: %d/%d", logit_nans, case.choice_logits.size)
            if log_prob_nans > 0:
                tf.logging.error("log_prob NaNs: %d/%d", log_prob_nans, case.choice_log_probs.size)
        return fetched["values"]

    def world_embeds(self, cases, answers, ignore_previous_utterances=False):
        if len(cases) == 0:
            # Should only happen if everything gets pruned off beam.
            return []
        fetch = {"world": self._world_embeds, "curr_world_flat": self._curr_world_embeds_flat,
                 "answer_flat": self._answer_embeds_flat}
        fetched = self.compute(
            fetch, cases, answers, rewards=None,
            ignore_previous_utterances=ignore_previous_utterances)
        return fetched

    def fut(self, cases, answers, ignore_previous_utterances=False):
        if len(cases) == 0:
            # Should only happen if everything gets pruned off beam.
            return []
        fetch = {"fut_embds": self.fut_embeds}
        fetched = self.compute(
            fetch, cases, answers, rewards=None,
            ignore_previous_utterances=ignore_previous_utterances)
        return fetched["fut_embds"]

    def grads_norms(self, vf_examples):
        if len(vf_examples) == 0:
            return 0.0
        cases, answers, rewards = self._unpack_vf_examples(vf_examples)
        return self.compute(self._norms, cases, rewards, ignore_previous_utterances=False)

    def loss(self, vf_examples):
        if len(vf_examples) == 0:
            return 0.0
        cases, answers, rewards = self._unpack_vf_examples(vf_examples)
        return self.compute(self._loss, cases, answers, rewards, ignore_previous_utterances=False)

    def get_loss_tensor(self):
        return self._loss

    def train_step(self, vf_examples, dbg=False):
        # Make sure all rewards are between [0, 1] for log_loss
        for ex in vf_examples:
            assert 0 <= ex.reward <= 1

        if len(vf_examples) == 0:
            print >> sys.stderr, " WARNING: (ValueFunction) Zero cases   \033[F"
            print("zero cases!")
        else:
            print >> sys.stderr, \
            " Updating (ValueFunction) ({} cases)   \033[F".format(
                len(vf_examples))

            cases, answers, rewards = self._unpack_vf_examples(vf_examples)

            # Always acknowledge previous utterances on train steps
            self.compute(
                self._take_step, cases, answers, rewards,
                ignore_previous_utterances=False)

    def pre_train_step(self, vf_examples, dbg=False):
        # Make sure all rewards are between [0, 1] for log_loss
        for ex in vf_examples:
            assert 0 <= ex.reward <= 1

        if len(vf_examples) == 0:
            print >> sys.stderr, " WARNING: (ValueFunction) Zero cases   \033[F"
            print("zero cases!")
        else:
            print >> sys.stderr, \
            " Updating (ValueFunction) ({} cases)   \033[F".format(
                len(vf_examples))

            cases, answers, rewards = self._unpack_vf_examples(vf_examples)

            # Always acknowledge previous utterances on train steps
            self.compute(
                self._take_pre_step, cases, answers, rewards,
                ignore_previous_utterances=False)

    def inputs_to_feed_dict(self, cases, answers, rewards=None,
                            ignore_previous_utterances=False):
        feed = {}
        if rewards:
            feed[self._rewards] = rewards

        world_stacks = [[case._previous_cases[-1].denotation.world_state.all_objects, answer[0].state.all_objects]
                        if len(case._previous_cases) > 0 else
                        [case.context.world.initial_state.all_objects, answer[0].state.all_objects]
                        for case, answer in zip(cases, answers)]
        feed.update(self._parse_model.inputs_to_feed_dict(
            cases, answers, ignore_previous_utterances, caching=False))
        feed.update(self._world_embedder.inputs_to_feed_dict(world_stacks))
        return feed


def build_world_embedder(config, name='target_primitive_embeds'):
    primitive_dim = config.primitive_dim
    sub_domain = config.sub_domain
    primitive_embeddings = RLongPrimitiveEmbeddings(primitive_dim)
    primitive_embedder = TokenEmbedder(primitive_embeddings, name, trainable=True)
    attrib_extractors = [lambda obj: obj.position]
    if sub_domain == 'scene':
        attrib_extractors.append(lambda obj: obj.shirt)
        attrib_extractors.append(lambda obj: obj.hat)
        pass
    elif sub_domain == 'alchemy':
        # skipping chemicals attribute for now, because it is actually a list
        attrib_extractors.append(lambda obj: obj.color if obj.color is not None else 'color-na')
        attrib_extractors.append(lambda obj: obj.amount)
    elif sub_domain == 'tangrams':
        attrib_extractors.append(lambda obj: obj.shape)
    elif sub_domain == 'undograms':
        attrib_extractors.append(lambda obj: obj.shape)
    max_list_size = config.max_list_size
    max_stack_size = 2
    return RLongObjectEmbedder(attrib_extractors, primitive_embedder, max_stack_size, max_list_size)


def get_value_function(config, parse_model):
    """Needs to take the Config for ValueFunction"""
    if config.type == "constant":
        return ConstantValueFunction(config.constant_value)
    elif config.type == "logistic":
        world_embeder = build_world_embedder(config)
        return LogisticValueFunction(
            parse_model, world_embeder, config.primitive_dim, config.learning_rate,
            OptimizerOptions(config.optimizer))
    else:
        raise ValueError(
            "ValueFunction {} not supported.".format(config.value_function.type))
