## Introduction

**Authors**: Dor Muhlgay, Jonathan Herzig, Jonathan Berant 

Source code accompanying our paper,
[Value-based Search in Execution Space for Mapping Instructions to Programs](https://arxiv.org/abs/1811.01090).

Also see:
- [An introduction to SCONE](https://nlp.stanford.edu/projects/scone/),
the context-dependent semantic parsing dataset that we evaluate on.
- [From Language to Programs: Bridging Reinforcement Learning and Maximum Marginal Likelihood](https://arxiv.org/abs/1704.07926),
the paper (by Guu et al) our research is based on.
 
## Setup

First, download the repository and necessary data.

```bash
$ git clone https://gitlab.com/tau-nlp/vbsix-lang2program
$ mkdir -p vbsix-lang2program/data
$ cd vbsix-lang2program/data
$ wget http://nlp.stanford.edu/data/glove.6B.zip  # GloVe vectors
$ unzip glove.6B.zip -d glove.6B
$ wget https://nlp.stanford.edu/projects/scone/scone.zip  # SCONE dataset
$ unzip scone.zip
```

The resulting data directory should look like this:

- data/
    - glove.6B/
    - rlong/

Create a virtualenv (e.g., in dir scone_env):

```bash
  $ cd ..  # navigate back to /vbsix-lang2program 
  $ virtualenv --system-site-packages scone_env
  $ source ./scone_env/bin/activate
```

Install requirements:

```bash
  (scone_env)$ easy_install -U pip 
  (scone_env)$ pip install -r ./requirements.txt 
```

Export environment (replace {path} with your directory):

```bash
$ export PYTHONPATH='/{path}/vbsix-lang2program'
$ export STRONGSUP_DIR='/{path}/vbsix-lang2program/data'
```


## Training a model

To launch a new training with VBSIX run:

```bash
(scone_env)$ cd /vbsix-lang2program 
(scone_env)$ python ./scripts/main.py --random-seed=0 configs/rlong/default-base.txt configs/rlong/config-mixins/only-use-stack-emb.txt configs/rlong/dataset-mixins/scene.txt configs/rlong/config-mixins/graph-beam-search.txt ./configs/rlong/config-mixins/value-search.txt # Train with VBSIX
```

To train the model with a different configuration, modify `configs/rlong/default-base.txt` and the configurations files in `configs/rlong/config-mixins/`
and `configs/rlong/dataset-mixins/`. These files are in [HOCON](https://github.com/typesafehub/config/blob/master/HOCON.md)
syntax.

On stdout, the script will print out the experiment's ID number.
Data for this experiment will be saved to the directory
`/vbsix-lang2program/data/experiments/<experiment_id>`, containing the following
files:

- config.txt
    - The config file for this training run
- checkpoints/
    - TensorFlow checkpoints, saved during training
- tensorboard/
    - TensorBoard log files

## Reproducing experiments 

Reproducible experiments are in `/vbsix-lang2program/paper_experiments/experiments/`, organized according to their domain, search algorithm, and random-seed.
Each experiment's directory contains its data as described above. 
The directory `/vbsix-lang2program/paper_experiments/code/` holds two versions of the source code: 
- strongsup_baseline
    - The [source code](https://github.com/kelvinguu/lang2program) accompanying the paper "[From Language to Programs: Bridging Reinforcement Learning and Maximum Marginal Likelihood](https://arxiv.org/abs/1704.07926)". This code should be used to reproduce the basleine beam-search experiments. 
- strongsup_vbsix
    - The source code accompanying our paper. This code should be used to reproduce the VBSIX and ablations experiments.

Reproduce an experiment by running:

```bash
(scone_env)$ mkdir ./data/experiments/{id}  # Create new experiment directory with id of your choice
(scone_env)$ cp -r ./paper_experiments/experiments/{domain}/{algorithm}/{random seed}/* ./data/experiments/{id}/ # Copy the experiments files 
(scone_env)$ rm -r ./strongsup/* # Replace the source code with the appropriate version (beam-search - strongsup_baseline, o.w - strongsup_vbsix) 
(scone_env)$ cp -r ./paper_experiments/code/{source code}/* ./strongsup/ 
(scone_env)$ python ./scripts/set_checkpoints.py -e {id}  # Generate checkpoint log file
(scone_env)$ python ./scripts/main.py {id} -m eval -s {train step number} -r {random seed} -c False
(scone_env)$ rm -r ./strongsup/*  # Copy back the vbsix version of the source code 
(scone_env)$ cp -r ./paper_experiments/code/strongsup_vbsix/* ./strongsup/ 
```

## Program syntax

See `scone.md` for more information.
