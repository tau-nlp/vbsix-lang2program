decoder {
    train_exploration_policy {
        type = "value-beam-search"
        exploration_gamma = null
        exploration_epsilon = 0.15
        beam_size = 32
        independent_utterance_exploration = false
        iterations_per_utterance = 7
        value_weight = 0.5
        use_afterstate = true
        value_ranking_start_step = 5000
        first_selection_size = 128
    }
    value_function {
        type = "logistic"
        optimizer = adam
        learning_rate = 0.001
        primitive_dim = 32
        max_list_size = 7
    }
}
